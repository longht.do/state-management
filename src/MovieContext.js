import React, { useState, createContext } from 'react';

export const MovieContext = createContext();

export const MovieProvider = (props) => {
  const [movies, setMovies] = useState([
    {
      name: 'Harry Porter',
      price: '$10',
      id: 1
    },
    {
      name: 'Fast and Furious',
      price: '$30',
      id: 2
    },
    {
      name: 'King Kong',
      price: '$50',
      id: 3
    }
  ])
  return (
   <MovieContext.Provider value={[movies, setMovies]}>
     {props.children}
   </MovieContext.Provider>
  )
}

